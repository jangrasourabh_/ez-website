import React from 'react';
import './css/work.css'
import {Card} from 'react-bootstrap';
import Logo from './images/EZ Works Blue.png';
import Translation from './images/EZ.png';
import Data from './images/Data@4x.png';
import Audio from './images/AV@4x.png';
import Graphic from './images/Graphic Design@4x.png';
import PPT from './images/PPT@4x.png';
import Research from './images/Research@4x.png'
export default function work() {
    return (
        <>
            <div className="bg3 p-3">
                <img src={Logo} className="img1"></img>
                <h5 className="text-center text-light">A Suite Of Business Support Services</h5>
                <div className="container mt-5">
                    <div className="row d-flex justify-content-around" >
                    <Card className="border-0 col-12 col-lg-4 bg-transparent show" style={{ width: '10rem' }}>
                    <Card.Img variant="top" src={Translation}  className="rounded mx-auto d-block img3"  />
                        <Card.Body>
                            <Card.Title className="text-light text-center">Translation Services</Card.Title>
                        </Card.Body>
                        </Card>

                        <Card className="border-0  col-12 col-lg-4 bg-transparent show" style={{ width: '10rem' }}>
                    <Card.Img variant="top" src={Data} className="rounded mx-auto d-block img3" />
                        <Card.Body>
                            <Card.Title className="text-light text-center">Data Processing</Card.Title>
                        </Card.Body>
                        </Card>

                        <Card className="border-0 col-12 col-lg-4 bg-transparent show" style={{ width: '10rem' }} >
                    <Card.Img variant="top" src={Audio} className="rounded mx-auto d-block img3"  />
                        <Card.Body>
                        <Card.Title className="text-light text-center">Audio-Visual</Card.Title>
                        <Card.Title className="text-light text-center">Presentation</Card.Title>
                        </Card.Body>
                        </Card>
                    </div>
                    <div className="row d-flex justify-content-around">
                    <Card className="border-0 bg-transparent col-12 col-lg-4 show" style={{ width: '10rem' }}>
                    <Card.Img variant="top" src={Graphic} className="rounded mx-auto d-block img3" />
                        <Card.Body>
                            <Card.Title className="text-light text-center">Graphic Design</Card.Title>
                        </Card.Body>
                        </Card>

                        <Card className="border-0 bg-transparent col-12 col-lg-4 show" style={{ width: '10rem' }}>
                    <Card.Img variant="top" src={PPT} className="rounded mx-auto d-block img3"/>
                        <Card.Body>
                            <Card.Title className="text-light text-center">Presentation Design</Card.Title>
                        </Card.Body>
                        </Card>

                        <Card className="border-0 bg-transparent col-12 col-lg-4 show" style={{ width: '10rem' }}>
                    <Card.Img variant="top" src={Research} className="rounded mx-auto d-block img3"/>
                        <Card.Body>
                            <Card.Title className="text-light text-center">Research Analysis</Card.Title>
                        </Card.Body>
                        </Card>
                    </div>
                </div>
            </div>        
        </>
    )
}