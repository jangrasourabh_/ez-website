import React from 'react';
import './css/home.css';
import { Card, Img } from 'react-bootstrap'
import Logo from './images/EZ Transparent.png';
import nature from './images/nature.jpg';
import nature2 from './images/natur2.jpg';
import Footer from './footer';
import Work from './work';
import Craousal from './craousal';
import { FaAngleDoubleDown, FaSearch } from 'react-icons/fa';
export default function home() {
    return (
        <>
            <div className="conatiner-fluid">
                <div className="bg1 p-5">
                    <img src={Logo} className="logo show" alt="Logo" />
                    <br></br>
                    <h4 className="text-light text-center">Your Word is Our Command</h4>
                </div>
                <div className="bg2 p-3">
                    <div class="search  center text-center mt-5">
                        <input type="text" class="searchTerm text-light" placeholder="Tell Us What You're Looking For" />
                        <button type="submit" class="searchButton"><FaSearch />
                        </button>
                    </div>
                    <h4 className="text-light text-center mt-5">What Others Are Looking For</h4>
                    <h4 className="text-light text-center "><FaAngleDoubleDown /></h4>

                    <div className="container p-3">
                        <div className="row d-flex justify-content-around ">
                            <Card className="col-md-4 col-sm-12 mt-3 p-0" style={{ width: '20rem' }}>
                                <div className="img-hover-zoom ">
                                    <img variant="top" src={nature} style={{ width: "100%",height:"98%" }} />
                                </div>
                                <img src={Logo} className="position-absolute p-2" style={{ height: '60px', width: '60px' }} alt="Logo" />
                                <div className="ml-2">
                                    <div className="cardin position-absolute rounded">
                                        Tag1, Tag2, Tag3
                                </div>
                                </div>
                                <Card.Body>
                                    <Card.Title>We are always There for you Literally.All the time.All Around.</Card.Title>
                                    <Card.Text>
                                        Some quick example text to build on the card title and make up the bulk of
                                        the card's content.
                                </Card.Text>
                                </Card.Body>
                            </Card>


                            <Card className="col-md-4 col-sm-12 mt-3 p-0" style={{ width: '20rem' }}>
                                <div className="img-hover-zoom ">
                                    <img variant="top" src={nature} style={{ width: "100%",height:"98%" }} />
                                </div>
                                <img src={Logo} className="position-absolute p-2" style={{ height: '60px', width: '60px' }} alt="Logo" />
                                <div className="ml-2">
                                    <div className="cardin position-absolute rounded">
                                        Tag1, Tag2, Tag3
                                </div>
                                </div>
                                <Card.Body>
                                    <Card.Title>We are always There for you Literally.All the time.All Around.</Card.Title>
                                    <Card.Text>
                                        Some quick example text to build on the card title and make up the bulk of
                                        the card's content.
                                </Card.Text>
                                </Card.Body>
                            </Card>
                            <Card className="col-md-4 col-sm-12 mt-3 p-0" style={{ width: '20rem' }}>
                                <div className="img-hover-zoom ">
                                    <img variant="top" src={nature} style={{ width: "100%",height:"98%" }} />
                                </div>
                                <img src={Logo} className="position-absolute p-2" style={{ height: '60px', width: '60px' }} alt="Logo" />
                                <div className="ml-2">
                                    <div className="cardin position-absolute rounded">
                                        Tag1, Tag2, Tag3
                                </div>
                                </div>
                                <Card.Body>
                                    <Card.Title>We are always There for you Literally.All the time.All Around.</Card.Title>
                                    <Card.Text>
                                        Some quick example text to build on the card title and make up the bulk of
                                        the card's content.
                                </Card.Text>
                                </Card.Body>
                            </Card>
                        </div>

                        <div className="row d-flex justify-content-around" >
                            <Card className="col-md-4 col-sm-12 mt-3 p-0" style={{ width: '20rem' }}>
                                <div className="img-hover-zoom ">
                                    <img variant="top" src={nature2} style={{ width: "100%",height: "95%"}} />
                                </div>
                                <img src={Logo} className="position-absolute p-2" style={{ height: '60px', width: '60px' }} alt="Logo" />
                                <div className="ml-2">
                                    <div className="cardon position-absolute rounded">
                                        Tag1, Tag2, Tag3
                                </div>
                                </div>
                                <Card.Body>
                                    <Card.Title>We are always There for you Literally.All the time.All Around.</Card.Title>
                                    <Card.Text>
                                        Some quick example text to build on the card title and make up the bulk of
                                        the card's content.
                                </Card.Text>
                                </Card.Body>
                            </Card>


                            <Card className="col-md-4 col-sm-12 mt-3 p-0" style={{ width: '20rem' }}>
                                <div className="img-hover-zoom ">
                                    <img variant="top" src={nature2} style={{ width: "100%", height: "95%" }} />
                                </div>
                                <img src={Logo} className="position-absolute p-2" style={{ height: '60px', width: '60px' }} alt="Logo" />
                                <div className="ml-2">
                                    <div className="cardon position-absolute rounded">
                                        Tag1, Tag2, Tag3
                                </div>
                                </div>
                                <Card.Body>
                                    <Card.Title>We are always There for you Literally.All the time.All Around.</Card.Title>
                                    <Card.Text>
                                        Some quick example text to build on the card title and make up the bulk of
                                        the card's content.
                                </Card.Text>
                                </Card.Body>
                            </Card>
                            <Card className="col-md-4 col-sm-12 mt-3 p-0" style={{ width: '20rem' }}>
                                <div className="img-hover-zoom ">
                                    <img variant="top" src={nature2} style={{ width: "100%",height: "95%" }} />
                                </div>
                                <img src={Logo} className="position-absolute p-2" style={{ height: '60px', width: '60px' }} alt="Logo" />
                                 <div className="ml-2">
                                    <div className="cardon position-absolute rounded">
                                        Tag1, Tag2, Tag3
                                </div>
                                </div>
                                <Card.Body>
                                    <Card.Title>We are always There for you Literally.All the time.All Around.</Card.Title>
                                    <Card.Text>
                                        Some quick example text to build on the card title and make up the bulk of
                                        the card's content.
                                </Card.Text>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                </div>
                <Work />
                <Craousal />
                <Footer />
            </div>
        </>
    )
}