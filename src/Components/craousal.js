import React from 'react';
import { Carousel } from 'react-bootstrap';
import './css/home.css';
import Laser from './images/laser.jpg'
export default function craousal() {
    return (
        <>
            <div className="container-fluid bg2">
                <div className="p-3">
                <h3 className="mb-3 text-center" style={{color:'orange'}}>Featured Articles</h3>
                    <Carousel>
                    <Carousel.Item>
                            <div className="row p-2">
                                <div className="col-sm-6">
                                    <img
                                        className="d-block"
                                        src={Laser}
                                        className="rounded"
                                        alt="First slide"
                                        style={{ width: '35rem' }}
                                    />
                                </div>
                                <div className="text-light  col-sm-6 " >
                                    <span className="h3">Title Goes Here Title Goes Here Title Goes Here Title Goes Here</span>
                                    <p className="">
                                        Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem
                                </p>
                                </div>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item>
                            <div className="row p-2">
                                <div className="col-12 col-lg-6">
                                    <img
                                        className="d-block"
                                        src={Laser}
                                        className="rounded"
                                        alt="First slide"
                                        style={{ width: '35rem' }}
                                    />
                                </div>
                                <div className="text-light col-12 col-lg-6" >
                                    <span className="h3">Title Goes Here Title Goes Here Title Goes Here Title Goes Here</span>
                                    <p className="">
                                        Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem
                                </p>
                                </div>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item>
                            <div className="row p-2">
                                <div className="col-sm-6">
                                    <img
                                        className="d-block"
                                        src={Laser}
                                        className="rounded"
                                        alt="First slide"
                                        style={{ width: '35rem' }}
                                    />
                                </div>
                                <div className="text-light col-sm-6 " >
                                    <span className="h3">Title Goes Here Title Goes Here Title Goes Here Title Goes Here</span>
                                    <p className="">
                                        Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem Lorem
                                </p>
                                </div>
                            </div>
                        </Carousel.Item>
                    </Carousel>
                </div>
            </div>
        </>
    )
}