import React from 'react';
import './css/home.css';
import Logo from './images/EZ Transparent.png';
import { FaInstagram,FaFacebook,FaTwitter,FaLinkedin,FaRegCopyright} from 'react-icons/fa';
export default function footer(){
        return(
            <>
            <div className="container-fluid p-3 bg1">
                <div className="row">
                    <div  className="col-md-3 col-sm-12">
                        <ul className="p-0">
                            <li className="h4">About EZ</li>  
                            <li>Our Journey</li>
                            <li>Investers</li>
                            <li>Information Security</li>
                        </ul>
                    </div>
                    <div  className="col-md-3 col-sm-12">
                        <ul className="p-0">
                        <li className="h4">Services</li>  
                            <li>Presentation Design</li>
                            <li>Translation Services</li>
                            <li>Audio-Visual Production</li>
                            <li>Graphic Design</li>
                            <li>Research & Analytics</li>
                            <li>Data Processing</li>
                        </ul>
                    </div>
                    <div  className="col-md-3 col-sm-12">
                        <ul className="p-0">
                        <li className="h4">People</li>  
                            <li>Ez Life</li>
                            <li>Team EZ</li>
                            <li>Join EZ</li>
                        </ul>
                    </div>
                    <div  className="col-md-3 col-sm-12">
                    <ul className="p-0">
                        <li className="h4"><img src={Logo} className="show" alt="Logo" style={{width:"40px"}}/></li>
                        <li>Contact Us</li>
                        <li className="mt-1"><span><FaInstagram className="h4 text-light"/> </span><span><FaFacebook className="h4 text-light"/></span> <span><FaTwitter className="h4 text-light" /></span> <span><FaLinkedin className="h4 text-light"/></span></li>
                    </ul>
                    </div>
                </div>

                <div className="row mt-3 srh d-md-none">
                    <div className="col-sm-12 text-center">
                    <p>Privacy Policy</p>
                        <p>Terms of Use</p>
                        <FaRegCopyright className="h5"/> 2020 ArabEasy LLC
                    </div>  
                </div>

                <div className="row mt-3 srh d-none d-md-block">
                    <div className="p-2 ml-3 float-left">
                        <FaRegCopyright className="h5"/> 2020 ArabEasy LLC
                    </div>
                    <div className="p-2  mr-3 text-right">
                        <span>Privacy Policy</span><span className="ml-3">Terms of Use</span>
                    </div>
                </div>

            </div>
            </>
        )
}